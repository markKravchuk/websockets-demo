import React from "react";
import io from 'socket.io-client';

//saving the socket in state, to be able to send message everywhere where state is declared
const state = {
    socket: {},
    username: ''
};

class DataComponent extends React.Component {

    componentDidMount() {
        //set username on input listener
        let username = document.getElementById('username');
        username.addEventListener('input', (e) => {
            state.username = username.value;
        });

        //creating socket connection
        state.socket = io('http://localhost:5000');

        //when connected
        state.socket.on('connect', function () {
            //make button text = "connected"
            let button = document.getElementById('status');
            button.innerText = "Connected";
        });

        //if received message from server
        state.socket.on('message', (data) => {
            //if this client was the sender
            if (data.username === state.username) data.username = 'Me';

            //adding new message to the message list
            let messages = this.state.messages;
            messages.push(data);
            this.setState({messages: messages})
        });

        //when the connection with socket server is lost
        state.socket.on('disconnect', function () {
            let button = document.getElementById('status');
            button.innerText = "NOT Connected";
        })
    }

    state = {
        messages: []
    };

    render() {
        return (
            <div>
                <p>Status: </p>
                <button id={"status"}>Not connected</button>

                <br/><br/>
                <label for="username">Enter your username:</label>
                <input type={"text"} placeholder={"Username"} id={"username"}/>

                <h5>Messages</h5>

                <ul id={"messages"}>
                    {this.state.messages.map((message) => {
                        return <li>{message.username} : {message.text}</li>
                    })}
                </ul>

                <input type={"text"} placeholder={"message to send"} id={"message"}/>

                <button id={"send"} onClick={this.sendMessage}>Send message</button>

            </div>
        )
    }

    sendMessage() {
        //get the field with the message
        let input = document.getElementById('message');

        //send to the server
        state.socket.emit('message', {
            username: state.username,
            text: input.value
        });

        //make the filed back empty
        input.value = '';
    }
}

export default DataComponent;