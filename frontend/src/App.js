import './App.css';
import DataComponent from "./DataComponent";
import React from "react";

function App() {
  return (
      <div>
          <DataComponent/>
      </div>
  );
}

export default App;
