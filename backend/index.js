const io = require('socket.io');
const express = require('express');

const app = express();
const server = app.listen(5000, function(){
    console.log('listening for requests on port 5000');
});

// Socket setup & pass server
const server_socket = io(server);

server_socket.on('connection', (socket) => {
    // When client sends a message
    socket.on('message', function(data){
        console.log("Message: " + JSON.stringify(data));
        //send new message to everyone connected
        server_socket.sockets.emit('message',data);
    });

    //if client disconnected
    socket.on('disconnect', function () {
        console.log("Closed 1 connection");
    });
});
